<!DOCTYPE html>
<html lang="es">
<head>
	<title>Delimitadores de codigo php</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/tabs.css">
		<link rel="stylesheet" type="text/css" href="css/delimeters.css">
</head>
<body>
<header>
	<h1>Los delimitadoe de codigo PHP</h1>
</header>
<section>
	<article>
		<div class="contenedor-tabs">
			<?php
			echo "<span class=\"diana\" id=\"una\"></span>\n";
			echo "<div class=\"tab\">\n";
			echo "<a href=\"#una\" class=\"tab-e\"> Estilo XML</a>\n";
echo "<div class=\"first\">\n";
echo "<p class=\"xmltag\">\n";

echo "este texto esta escrito en php, utilizando etiquetas más";
echo "usuales y recomendados para delimitar el codigo php, que son: ";
echo "&lt;php ...&gt;.<br>\n";
echo "</p>\n";
echo "</div>\n";
echo "</div>\n";
?>
<?php
echo "<span class=\"diana\" id=\"tres\"></span>\n";
echo "<div class=\"tab\">\n";
echo "<a href=\"#tres\" class=\"tab-e\">Etiquetas cortas </a>";
echo "</div>\n";

echo "<p class=\"shorttag\">";
echo "este texto esta escrito tambien con php utilizando etiquetas";
echo "cortas, </br>\n que son: &lt;?...? 
&gt;";

echo "</p>\n";
echo "</div>\n";
			
			?>
		</div>
	</article>
</section>
</body>
</html>